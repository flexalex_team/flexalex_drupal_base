<?php

namespace flexalex;


class Context {

  static private $items = array();

  public static function get($module, $name) {
    $key = "$module-$name";
    if (!array_key_exists($key, self::$items)) {
      self::$items[$key] = self::getNew($module, $name);
    }
    return self::$items[$key];
  }

  private static function getNew($module, $name) {
    $item = module_invoke($module, 'flexalex_context_get', $name);
    if (is_null($item)) {
      throw new \Exception("Error getting instance for $module / $name");
    }
    return $item;
  }
}